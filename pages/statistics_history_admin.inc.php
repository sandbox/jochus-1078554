<?php

/**
 * Shows the administration form for this module
 */
function statistics_history_admin() {
	$form = array();
	
	/******************************************************************************************************************
	 * MOST READ
	 *****************************************************************************************************************/
	$form['most_read'] = array(
		'#type' => 'fieldset',
		'#title' => 'Most read content',
	    	'#collapsible' => TRUE,
    		'#collapsed' => TRUE,
	);
	
	// title
	$form['most_read']['statistics_history_most_read_title'] = array(
		'#type' => 'textfield',
		'#title' => t('What should be the title of this block?'),
		'#default_value' => variable_get('statistics_history_most_read_title', 'Most read'),
		'#maxlength' => 500,
		'#size' => 120,
		'#required' => TRUE,
	);
	
	// total lines
	$form['most_read']['statistics_history_most_read_total_lines'] = array(
		'#type' => 'textfield',
		'#title' => t('How many lines should be presented to the user?'),
		'#default_value' => variable_get('statistics_history_most_read_total_lines', '5'),
		'#maxlength' => 500,
		'#size' => 120,
		'#required' => TRUE,
	);

	// period
	$period_options = array();
	for ($i = 1; $i <= 60; $i++)  {
		$period_options[$i] = $i;
	}
	$form['most_read']['statistics_history_most_read_period'] = array(
		'#type' => 'select',
		'#title' => t('How many day should we consider (counted from today, backwards)?'),
		'#default_value' => variable_get('statistics_history_most_read_period', 30),
		'#options' => $period_options,
		'#required' => TRUE,
	);
	
	// node types
    	$form['most_read']['statistics_history_most_read_types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Only of the type(s)'),
		'#prefix' => '<div class="criterion">',
		'#suffix' => '</div>',
		'#options' => node_get_types('names'),
		'#default_value' => variable_get('statistics_history_most_read_types', NULL),
		'#required' => TRUE,
    	);

	/******************************************************************************************************************
	 * HOT TOPICS
	 *****************************************************************************************************************/
	$form['hot_topics'] = array(
		'#type' => 'fieldset',
		'#title' => 'Most commented content',
    	'#collapsible' => TRUE,
    	'#collapsed' => TRUE,
	);
	
	// title
	$form['hot_topics']['statistics_history_hot_topics_title'] = array(
		'#type' => 'textfield',
		'#title' => t('What should be the title of this block?'),
		'#default_value' => variable_get('statistics_history_hot_topics_title', 'Hot topics'),
		'#maxlength' => 500,
		'#size' => 120,
		'#required' => TRUE,
	);
	
	// total lines
	$form['hot_topics']['statistics_history_hot_topics_total_lines'] = array(
		'#type' => 'textfield',
		'#title' => t('How many lines should be presented to the user?'),
		'#default_value' => variable_get('statistics_history_hot_topics_total_lines', '5'),
		'#maxlength' => 500,
		'#size' => 120,
		'#required' => TRUE,
	);

	// period
	$period_options = array();
	for ($i = 1; $i <= 60; $i++)  {
		$period_options[$i] = $i;
	}
	$form['hot_topics']['statistics_history_hot_topics_period'] = array(
		'#type' => 'select',
		'#title' => t('How many day should we consider (counted from today, backwards)?'),
		'#default_value' => variable_get('statistics_history_hot_topics_period', 30),
		'#options' => $period_options,
		'#required' => TRUE,
	);

	return system_settings_form($form);	
}
